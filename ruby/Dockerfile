FROM jpickwell/debian:jessie

LABEL maintainer "Jordan Pickwell"

COPY root /

ENV RUBY_INSTALL_VERSION=0.6.1 \
    RUBY_VERSION=2.4.1

RUN cd /tmp \
 && wget https://raw.github.com/postmodern/postmodern.github.io/master/postmodern.asc \
 && gpg --import postmodern.asc \
 && rm -f postmodern.asc \
 && wget \
      -O ruby-install-${RUBY_INSTALL_VERSION}.tar.gz \
      https://github.com/postmodern/ruby-install/archive/v${RUBY_INSTALL_VERSION}.tar.gz \
 && wget https://raw.github.com/postmodern/ruby-install/master/pkg/ruby-install-${RUBY_INSTALL_VERSION}.tar.gz.asc \
 && gpg --verify \
      ruby-install-${RUBY_INSTALL_VERSION}.tar.gz.asc \
      ruby-install-${RUBY_INSTALL_VERSION}.tar.gz \
 && tar -xzvf ruby-install-${RUBY_INSTALL_VERSION}.tar.gz \
 && rm -f \
      ruby-install-${RUBY_INSTALL_VERSION}.tar.gz \
      ruby-install-${RUBY_INSTALL_VERSION}.tar.gz.asc \
 && cd ruby-install-${RUBY_INSTALL_VERSION} \
 && make install \
 && cd / \
 && rm -fr /tmp/ruby-install-${RUBY_INSTALL_VERSION} \
 && apt-get update \
 && apt-get install -y --no-install-recommends --no-install-suggests \
      bison \
      libbison-dev \
 && ruby-install \
      --system -c -j$( cat /proc/cpuinfo | grep -c processor ) \
      ruby ${RUBY_VERSION} \
      -- --disable-install-rdoc \
 && gem update --system \
 && gem update -f rdoc \
 && gem update \
 && apt-get autoclean \
 && apt-get clean \
 && rm -fr /var/lib/apt/lists/*
